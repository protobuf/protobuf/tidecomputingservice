package org.veille;

import static spark.Spark.*;
import java.time.*;

import org.veille.protobuf.TideProto;
import org.veille.protobuf.TideProto.Tide;
import org.veille.protobuf.TideProto.TideHeight;

import com.google.gson.Gson;

public class Main {
    public static void main(String[] args) {

        // post route receive serialized message, parse it, add requested date,
        // serialize and return it
        post("/tide", (request, response) -> {
            // deserialize the result into an object (a message) to get data.
            TideProto.Tide receivedTide = TideProto.Tide.parseFrom(request.bodyAsBytes());

            int byteSizeRequestProto = request.contentLength();
            System.out.println("Proto request content size : " + byteSizeRequestProto);

            // Get the value of the requested height from a computing function
            float height = getHeight(receivedTide);

            // Create a builder for a new message of class TideHeight where we can set the
            // requestedHeight value
            // : builders are mutable, but not messages
            TideHeight.Builder th = TideHeight.newBuilder();
            TideHeight tideHeight = th.setTideHeight(height).build();

            // serialize the new message to return it in response
            byte[] bytes = tideHeight.toByteArray();

            return bytes;
        });

        // ---- Test with JSON ----//
        post("/tideJson", (request, response) -> {
            // deserialize the result into an object (a message) to get data.
            Gson gson = new Gson();
            TideObject receivedTide = gson.fromJson(request.body(), TideObject.class);

            int byteSizeRequestJson = request.contentLength();
            System.out.println("json request content size : " + byteSizeRequestJson);

            // Get the value of the requested height from a computing function
            float height = getHeightFromJson(receivedTide);

            // serialize the new message to return it in response
            String jsonStr = gson.toJson(height);
            return jsonStr;
        });
        // ---- ----//
    }

    /**
     * Computed height of tide at a specified time from data passed by server
     * 
     * @param tide object
     * @return height
     */
    public static Float getHeight(Tide tide) {
        float res = 0.0f;

        try {

            float previousTideHeight = tide.getPreviousTideHeight();
            float nextTideHeight = tide.getNextTideHeight();
            LocalDateTime requestedTime = OffsetDateTime.parse(tide.getRequestedDateTime()).toLocalDateTime();
            LocalDateTime previousTideDateTime = OffsetDateTime.parse(tide.getPreviousTideDateTime()).toLocalDateTime();
            LocalDateTime nextTideDateTime = OffsetDateTime.parse(tide.getNextTideDateTime()).toLocalDateTime();
            Long durationBetweenTides = Duration.between(previousTideDateTime, nextTideDateTime).toMinutes();
            Long durationSincePrevTide = Duration.between(previousTideDateTime, requestedTime).toMinutes();

            // We need to shift the graph horizontally based on the time since the previous
            // tide and we need to produce a negative number so that we can get a value
            // below the equilibrium
            Long timeShift = (durationSincePrevTide - (durationBetweenTides / 2)) * 2;

            // If the tide is going down we need to shift the graph the
            // other direction to catch the down slope
            int tideDirectionMultiplier = (previousTideHeight > nextTideHeight) ? -1 : 1;

            float tideHeightDifference = previousTideHeight + nextTideHeight;
            float averageTideHeight = tideHeightDifference / 2;

            float tideAmplitude = Math.max(Math.abs(averageTideHeight - previousTideHeight),
                    Math.abs(averageTideHeight - nextTideHeight));
            double currentHeight = (tideAmplitude
                    * Math.sin((Math.PI / (durationBetweenTides * 2)) * (timeShift * tideDirectionMultiplier)))
                    + averageTideHeight;

            res = (float) currentHeight;

        } catch (Exception error) {
            System.out.println("error : " + error);
        }

        return res;
    }

    /**
     * Computed height of tide at a specified time from data passed by server
     * 
     * @param tideObject object
     * @return height
     */
    public static Float getHeightFromJson(TideObject tide) {
        float res = 0.0f;

        try {

            float previousTideHeight = tide.getPreviousTideHeight();
            float nextTideHeight = tide.getNextTideHeight();
            LocalDateTime requestedTime = OffsetDateTime.parse(tide.getRequestedDateTime()).toLocalDateTime();
            LocalDateTime previousTideDateTime = OffsetDateTime.parse(tide.getPreviousTideDateTime()).toLocalDateTime();
            LocalDateTime nextTideDateTime = OffsetDateTime.parse(tide.getNextTideDateTime()).toLocalDateTime();
            Long durationBetweenTides = Duration.between(previousTideDateTime, nextTideDateTime).toMinutes();
            Long durationSincePrevTide = Duration.between(previousTideDateTime, requestedTime).toMinutes();

            // We need to shift the graph horizontally based on the time since the previous
            // tide and we need to produce a negative number so that we can get a value
            // below the equilibrium
            Long timeShift = (durationSincePrevTide - (durationBetweenTides / 2)) * 2;

            // If the tide is going down we need to shift the graph the
            // other direction to catch the down slope
            int tideDirectionMultiplier = (previousTideHeight > nextTideHeight) ? -1 : 1;

            float tideHeightDifference = previousTideHeight + nextTideHeight;
            float averageTideHeight = tideHeightDifference / 2;

            float tideAmplitude = Math.max(Math.abs(averageTideHeight - previousTideHeight),
                    Math.abs(averageTideHeight - nextTideHeight));
            double currentHeight = (tideAmplitude
                    * Math.sin((Math.PI / (durationBetweenTides * 2)) * (timeShift * tideDirectionMultiplier)))
                    + averageTideHeight;

            res = (float) currentHeight;

        } catch (Exception error) {
            System.out.println("error : " + error);
        }

        return res;
    }

}