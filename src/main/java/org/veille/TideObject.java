package org.veille;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TideObject {
    private @Getter @Setter float previousTideHeight;
    private @Getter @Setter float nextTideHeight;
    private @Getter @Setter String requestedDateTime;
    private @Getter @Setter String previousTideDateTime;
    private @Getter @Setter String nextTideDateTime;
}
