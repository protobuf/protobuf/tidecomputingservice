## Description
Ce service fait partie d'un POC pour tester la transmission de données entre un server Node.js Express et un service Java avec Protocol Buffers.

Il reçoit du server via une rquête http un objet sérialisé avec protobuf et contenant des informations de marée pour le port de Saint-Malo ainsi qu'une date et une heure précises. A partir de ces données, il calcule la hauteur d'eau à l'instant demandé, et retourne le résultat au server.


## Protobuf
Les messages échangés doivent être définis dans un fichier ".proto", qui constitue le schéma de validation des objects.

Ce fichier doit ensuite être compilé par le compilateur "protoc", précédemment installé sur l'ordinateur (https://github.com/protocolbuffers/protobuf/releases/tag/v21.12), avec la ligne de commande suivante :

``protoc -I=./ --java_out=./src/main/java ./src/tides.proto``


Son utilisation requiert également l'ajout d'une dépendance. Pour Maven : 

`` <dependencies>
    <dependency>
      <groupId>com.google.protobuf</groupId>
      <artifactId>protobuf-java</artifactId>
      <version>${indiquer ici la derniere version}</version>
    </dependency>
  </dependencies>
``

Tutoriel officiel pour java : https://developers.google.com/protocol-buffers/docs/javatutorial?hl=fr

